# easy-xlxd

XLXD MULTIMODE REFLECTOR ,customized modified script, allows to adjust in an easy way:

* style dashboard select clasic or new

* port number ysf reflector

* number of enabled modules

* default module YSF

* xlx reflector number or letters

* country

* reflector description xlxd

* ambe ssrver address

* ambe server port

* routine is included that auto restarts reflector xlxd in case of internet loss

#

# Install

    apt-get update
    
    apt-get install curl sudo -y
    
    bash -c "$(curl -fsSL https://gitlab.com/hp3icc/easy-xlxd/-/raw/main/xlx-install.sh)"
    
   
 To extend or change configuration values, just run the script again and configure according to your preference
 
#
  
 # Location files config
 
  * Dashboard 1 Clasic : 

  /opt/xlxd/dashboard

  * Dashboard 2 Dark :

  /opt/xlxd/dashboard

  * config Dashboard 1 Clasic :
 
  /opt/xlxd/dashboard/pgs/config.inc.php

  * config Dashboard 2 Dark :

  /opt/xlxd/dashboard/pgs/config.inc.php
  
  * Service :
  
  /etc/init.d/xlxd
  
  * interlink & other files :  
   
  /xlxd/  
  

#

# Firewall settings #

XLX Server requires the following ports to be open and forwarded properly for in- and outgoing network traffic:
 - TCP port 80            (http) optional TCP port 443 (https)
 - TCP port 8080          (RepNet) optional
 - UDP port 10001         (json interface XLX Core)
 - UDP port 10002         (XLX interlink)
 - TCP port 22            (ssh) optional  TCP port 10022
 - UDP port 42000         (YSF protocol)
 - UDP port 30001         (DExtra protocol)
 - UPD port 20001         (DPlus protocol)
 - UDP port 30051         (DCS protocol)
 - UDP port 8880          (DMR+ DMO mode)
 - UDP port 62030         (MMDVM protocol)
 - UDP port 10100         (AMBE controller port)
 - UDP port 10101 - 10199 (AMBE transcoding port)
 - UDP port 12345 - 12346 (Icom Terminal presence and request port)
 - UDP port 40000         (Icom Terminal dv port)
 - UDP port 21110         (Yaesu IMRS protocol)

#

# Credits

 * original scrip n5amd without modifications by hp3icc :

 https://github.com/n5amd/xlxd-debian-installer

 * Source xlxd files :
 
 https://github.com/LX3JL/xlxd
 
#

# Contributions

 I thank colleagues IU2NAF Diego and G7RZU Simon, for their time and information to successfully modify and customize this scrip at the service of all.
 
Have fun, good Qso, success in your projects

73.

HP3ICC

Esteban Mackay Q.
